package me.ford.droppickup.leftovers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.ford.droppickup.DropPickup;

public class PerPlayerInventoryHandler {
	private static final String FOLDER_NAME = "player_inv_dump";
	private final DropPickup DP;
	private final Map<UUID, PlayerLeftOverChest> cache = new HashMap<>();
	
	public PerPlayerInventoryHandler(DropPickup plugin) {
		DP = plugin;
	}
	
	public boolean addItems(Player player, List<ItemStack> items) {
		PlayerLeftOverChest chests = getPlayerChests(player);
		return chests.addItems(items);
	}
	
	public PlayerLeftOverChest getPlayerChests(Player player) {
		UUID id = player.getUniqueId();
		PlayerLeftOverChest chests = getCached(id);
		if (chests == null) {
			chests = new PlayerLeftOverChest(DP, FOLDER_NAME, player, getNrOfChests(player), DP.getSettings().getVirtualStorageSettings().getSizeOfChests());
			addToCache(id, chests);
		}
		return chests;
	}
	
	public Inventory getInventory(Player player, int nr) {
		return getPlayerChests(player).getBukkitInventory(nr);
	}
	
	public void updateInventory(Player player, int nr, Inventory inv) {
		getPlayerChests(player).setInventory(nr, inv.getContents());
	}
	
	public int getNrOfChests(Player player) {
		return DP.getSettings().getVirtualStorageSettings().getNrOfChests(player);
	}
	
	public void playerLeft(OfflinePlayer player) {
		removeFromCache(player.getUniqueId());
	}

	
	private void addToCache(UUID uuid, PlayerLeftOverChest profile) {
		cache.put(uuid, profile);
	}
	
	private PlayerLeftOverChest getCached(UUID uuid) {
		return cache.get(uuid);
	}
	
	private void removeFromCache(UUID uuid) { // log out
		cache.remove(uuid);
	}
	
	

}
