package me.ford.droppickup.leftovers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.droppickup.utils.CustomConfig;

public class PlayerLeftOverChest extends CustomConfig {
	private final UUID id;
	private final int nrOfChests;
	private final int slotsInChest;
	private int maxSaveDelay = 20;
	private boolean isScheduled = false;
	
	public PlayerLeftOverChest(JavaPlugin plugin, String folderName, Player player, int nrOfChests) {
		this(plugin, folderName, player, nrOfChests, 54);
	}

	public PlayerLeftOverChest(JavaPlugin plugin, String folderName, Player player, int nrOfChests, int slotsInChest) {
		super(plugin, folderName, player.getUniqueId().toString());
		id = player.getUniqueId();
		this.nrOfChests = nrOfChests;
		this.slotsInChest = slotsInChest;
	}
	
	private void saveInventory(int nr, List<ItemStack> inv) {
		getConfig().set(String.valueOf(nr), inv);
		scheduleSaving();
	}
	
	private void scheduleSaving() {
		if (isScheduled) {
			return;
		}
		// SCHEDULE
		isScheduled = true;
		getProvidingPlugin().getServer().getScheduler().runTaskLater(getProvidingPlugin(), () -> {
			saveConfig();
			isScheduled = false;
		} , maxSaveDelay * 20L);
	}
	
	public UUID getId() {
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItemStack> getInventory(int nr) {
		List<ItemStack> items;
		try {
			items = (List<ItemStack>) getConfig().getList(String.valueOf(nr));
		} catch (ClassCastException e) {
			getProvidingPlugin().getLogger().warning("Unable to parse list of ItemStacks for " + id + " chest #" + nr);
			return new ArrayList<>();
		}
		if (items == null) {
			return new ArrayList<>();
		} else {
			return items;
		}
	}
	
	public void setInventory(int nr, ItemStack[] contents) {
		List<ItemStack> items = new ArrayList<>();
		for (ItemStack item : contents) {
			if (item != null) items.add(item);
		}
		setInventory(nr, items);
	}
	
	public void setInventory(int nr, List<ItemStack> inv) {
		if (nr >= nrOfChests) throw new IllegalArgumentException("Only allowed " + nrOfChests + " chest, but trying to set chest #" + nr);
		saveInventory(nr, inv);
	}
	
	public boolean addItems(List<ItemStack> items) { // items left in the List did not fit
		if (items.isEmpty()) {
			return true;
		}
		for (int i = 0; i < nrOfChests; i++) {
			if (fillChest(i, items)) {
				return true;
			}
		}
		return false; 
	}
	
	private boolean fillChest(int i, List<ItemStack> items) {
		List<ItemStack> overflow = new ArrayList<>();
		List<ItemStack> inv = getInventory(i);
		boolean changes = true;
		for (ItemStack curAdd : items) {
			int max = curAdd.getMaxStackSize();
			int addAmount = curAdd.getAmount();
			boolean done = populate(inv, curAdd, addAmount, max); // current slots
			if (!done) { // NEW SLOT
				 done = addToNewSlot(inv, curAdd);
			}
			if (addAmount > curAdd.getAmount()) {
				changes = true;
			}
			if (!done) {
				overflow.add(curAdd);
			}
		}
		if (changes) {
			setInventory(i, inv);
		}
		items.clear();
		items.addAll(overflow);
		return items.isEmpty();
	}
	
	private boolean addToNewSlot(List<ItemStack> inv, ItemStack add) {
		if (inv.size() >= slotsInChest) {
			return false;
		}
		inv.add(add);
		return true;
	}
	
	private boolean populate(List<ItemStack> inv, ItemStack add, int amount, int max) {
		for (ItemStack curInv : inv) {
			if (curInv.isSimilar(add) && curInv.getAmount() < max) {
				amount = addToItem(curInv, amount, max);
				add.setAmount(amount);
				if (amount == 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	private int addToItem(ItemStack to, int add, int max) {
		int from = to.getAmount();
		int newAmount = Math.min(max, from + add);
		to.setAmount(newAmount); // newAmount - from = added, add - added = left; left = add + from - newAmount
		return add + from - newAmount;
	}
	
	public Inventory getBukkitInventory(int i) {
		if (i >= nrOfChests) return null;
		List<ItemStack> contents = getInventory(i);
		Inventory inv = Bukkit.createInventory(null, slotsInChest);
		inv.addItem(contents.toArray(new ItemStack[0]));
		return inv;
	}

}
