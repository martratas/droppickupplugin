package me.ford.droppickup.leftovers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.permissions.Permissible;

import me.ford.droppickup.DropPickup;

public class VirtualStorageSettings {
	private static final String PATH = "virtual-storage";
	private final DropPickup DP;
	
	public VirtualStorageSettings(DropPickup plugin) {
		DP = plugin;
	}
	
	private ConfigurationSection getConfig() {
		return DP.getConfig().getConfigurationSection(PATH);
	}
	
	public boolean destroyOverflow() {
		return getConfig().getBoolean("destroy-overflow", true);
	}
	
	public int defaultChests() {
		return getConfig().getInt("default-nr-of-chests", 1);
	}
	
	public int getSizeOfChests() {
		return getConfig().getInt("size-of-chests", 54);
	}
	
	private Map<String, Integer> getPermMap() {
		Map<String, Integer> map = new HashMap<>();
		ConfigurationSection section = getConfig().getConfigurationSection("chest-nr-permissions");
		for (String key : section.getKeys(false)) {
			map.put("droppickup.virtualstorage.extra." + key, section.getInt(key));
		}
		return map;
	}
	
	public int getNrOfChests(Permissible perm) {
		if (!perm.hasPermission("droppickup.virtualstorage")) return 0;
		int max = defaultChests();
		for (Entry<String, Integer> entry : getPermMap().entrySet()) {
			if (perm.hasPermission(entry.getKey())) {
				max = Math.max(max, entry.getValue());
			}
		}
		return max;
	}

}
