package me.ford.droppickup.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

//import org.apache.commons.lang.Validate;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomConfig {
	private final JavaPlugin plugin;
	private final String fileName;
	private final File folder;
	private File file = null;
	private FileConfiguration config = null;
	
	public CustomConfig(JavaPlugin plugin, String name) {
		this(plugin, null, name);
	}
	
	public CustomConfig(JavaPlugin plugin, String folderName, String name) {
//		Validate.notNull(plugin, "Plugin cannot be null!");
//		Validate.notNull(name, "Name cannot be null!");
//		Validate.notEmpty(name, "Name cannot be empty");
//		File fFolder;
		if (folderName != null) {
			folder = new File(plugin.getDataFolder(), folderName);
			if (!folder.exists()) {
				folder.mkdir();
			}
		} else {
			folder = plugin.getDataFolder();
		}
		if (!name.toLowerCase().endsWith(".yml")) {
			name += ".yml";
		}
		this.plugin = plugin;
		this.fileName = name;
	}
	
	public void reload() {
	    if (file == null) {
	    	file = new File(folder, fileName);
	    }
	    config = YamlConfiguration.loadConfiguration(file);

	    // Look for defaults in the jar
	    InputStream is = plugin.getResource(fileName);
	    Reader defConfigStream = null;
	    if (is != null) {
			try {
				defConfigStream = new InputStreamReader(is, "UTF8");
			} catch (UnsupportedEncodingException e) {
				defConfigStream = null;
			}
	    }
	    if (defConfigStream != null) {
	        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	        config.setDefaults(defConfig);
	    }
	}
	
	public FileConfiguration getConfig() {
	    if (config == null) {
	        reload();
	    }
	    return config;
	}
	
	public void saveConfig() {
	    if (config == null || file == null) {
	        return;
	    }
	    try {
	        getConfig().save(file);
	    } catch (IOException ex) {
	    	plugin.getLogger().log(Level.SEVERE, "Could not save config to " + file, ex);
	    }
	}
	
	public JavaPlugin getProvidingPlugin() {
		return plugin;
	}
	
	public String getFileName() {
		return fileName;
	}

}
