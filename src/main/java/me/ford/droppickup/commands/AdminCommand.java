package me.ford.droppickup.commands;

import java.util.List;
import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import com.google.common.collect.Lists;

import me.ford.droppickup.DropPickup;

public class AdminCommand implements TabExecutor {
	public static final List<String> SUB_COMMANDS = Lists.asList("reload", new String[] {});
	private final DropPickup DP;
	
	public AdminCommand(DropPickup plugin) {
		DP = plugin;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			return StringUtil.copyPartialMatches(args[0], SUB_COMMANDS, list);
		}
		return list;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			DP.reload();
			sender.sendMessage(DP.getSettings().getConfigReloadMessage());
			return true;
		}
		return false;
	}

}
