package me.ford.droppickup.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.util.StringUtil;

import me.ford.droppickup.DropPickup;

public class BaseCommand implements TabExecutor {
	private final List<String> subCommandNames = new ArrayList<>();
	private Map<String, SubCommand> subCommands = new HashMap<>();
	
	public BaseCommand(DropPickup plugin) {
		subCommands.put("profile", new ProfileCommand(plugin));
		subCommands.put("help", new HelpCommand(subCommands.values()));
		SubCommand cmd = new VirtualChestCommand(plugin);
		subCommands.put("virtualstorage", cmd);
		subCommands.put("vs", cmd);
		cmd = new VirtualChestClearCommand(plugin);
		subCommands.put("virtualstorageclear", cmd);
		subCommands.put("vsc", cmd);
		subCommandNames.addAll(subCommands.keySet());
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			return StringUtil.copyPartialMatches(args[0], subCommandNames, list);
		} else {
			SubCommand cmd = subCommands.get(args[0].toLowerCase());
			if (cmd != null) {
				return cmd.onTabComplete(sender, command, alias, args);
			}
		}
		return list;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			return false;
		}
		SubCommand cmd = subCommands.get(args[0].toLowerCase());
		if (cmd == null) {
			return false;
		}
		return cmd.onCommand(sender, command, label, args);
	}

}
