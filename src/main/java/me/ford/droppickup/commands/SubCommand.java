package me.ford.droppickup.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class SubCommand implements TabExecutor {
	
	public String getUsage() {
		return getUsage(false);
	}
	
	public Player getTarget(CommandSender sender, String name, JavaPlugin plugin) {
		Player target = null;
		if (name == null) {
			if (sender instanceof Player) {
				target = (Player) sender;
			} // else stays null
		} else {
			target = plugin.getServer().getPlayer(name);
			if (target == null && sender instanceof Player) {
				target = (Player) sender;
			} // else either stays as target or null
		}
		return target;
	}
	
	public abstract String getUsage(boolean admin);

}
