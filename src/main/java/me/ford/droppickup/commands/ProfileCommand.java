package me.ford.droppickup.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.ford.droppickup.DropPickup;
import me.ford.droppickup.profiles.Profile;
import me.ford.droppickup.profiles.ProfileHandler;

public class ProfileCommand extends SubCommand {
	private static final String USAGE = "/dp profile <profile>";
	private static final String USAGE_ADMIN = "/dp profile <profile> [player]";
	private final DropPickup DP;
	
	public ProfileCommand(DropPickup plugin) {
		DP = plugin;
	}
	
	public String getUsage(boolean admin) {
		if (!admin) {
			return USAGE;
		} else {
			return USAGE_ADMIN;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<String>();
		if (args.length == 2) {
			return StringUtil.copyPartialMatches(args[1], DP.getProfileHandler().getProfileNames(), list);
		}
		return list;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("droppickup.profile")) {
			sender.sendMessage(DP.getSettings().getNoPermissionMessage());
			return true;
		}
		boolean canOthers = sender.hasPermission("droppickup.profile.others"); 
		if (args.length < 2) {
			if (!canOthers) {
				sender.sendMessage(USAGE);
			} else {
				sender.sendMessage(USAGE_ADMIN);
			}
			return true;
		}
		String targetName = canOthers ? (args.length > 2 ? args[2]:null): null;
		Player target = getTarget(sender, targetName, DP);
		if (target == null) { // only if targetName is null or cannot be found AND sender is CONSOLE
			if (targetName == null) { // CONSOLE didn't specify target name
				sender.sendMessage(USAGE_ADMIN);
			} else { // this shouldn't happen - if target wasn't found then it reverts to sender if instance of player
				sender.sendMessage(DP.getSettings().getTargetNotFoundMessage(targetName));
			}
			return true;
		}
		
		ProfileHandler handler = DP.getProfileHandler();
		// profile
		Profile newProfile = handler.getProfile(args[1]);
		if (newProfile == null) {
			sender.sendMessage(DP.getSettings().getProfileNotFoundMessage(args[1]));
			return true;
		}
		
		Profile cur = handler.getPlayerProfile(target);
		if (cur == newProfile) {
			sender.sendMessage(DP.getSettings().getAlreadyOnProfileMessage(cur.getName()));
			return true;
		}
		
		// change
		handler.setPlayerProfile(target, newProfile);
		if (target == sender) {
			sender.sendMessage(DP.getSettings().getSetProfileFromToMessage(cur.getName(), newProfile.getName()));
		} else {
			sender.sendMessage(DP.getSettings().getSetProfileFromToOtherMessage(target.getName(), cur.getName(), newProfile.getName()));
		}
		return true;
	}

}
