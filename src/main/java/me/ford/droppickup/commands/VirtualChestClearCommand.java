package me.ford.droppickup.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import me.ford.droppickup.DropPickup;
import me.ford.droppickup.leftovers.PerPlayerInventoryHandler;

public class VirtualChestClearCommand extends SubCommand {
	private static final String USAGE = "/dp vsc <nr>";
	private static final String USAGE_ADMIN = "/dp vsc <nr> [player]";
	private static final String ADMIN_PERMS = "droppickup.virtualstorage.clear.other";
	private final DropPickup DP;
	
	public VirtualChestClearCommand(DropPickup plugin) {
		DP = plugin;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 2) {
			for (int i = 1; i <= DP.getSettings().getVirtualStorageSettings().getNrOfChests(sender); i++) {
				list.add(String.valueOf(i));
			}
		} else if (args.length == 3 && sender.hasPermission(ADMIN_PERMS)) {
			return null; // players
		}
		return StringUtil.copyPartialMatches(args[args.length - 1], list, new ArrayList<>());
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(DP.getSettings().getPlayerCommandMessage());
			return true;
		}
		Player player = (Player) sender;
		if (!sender.hasPermission("droppickup.virtualstorage.command")) {
			sender.sendMessage(DP.getSettings().getNoPermissionMessage());
			return true;
		}
		PerPlayerInventoryHandler handler = DP.getInventoryHandler();
		if (handler == null) {
			sender.sendMessage(DP.getSettings().getVirtualStorageNotEnabledMessage());
			return true;
		}
		boolean canOthers = sender.hasPermission("droppickup.profile.others"); 
		if (args.length < 2) {
			sender.sendMessage(getUsage(canOthers));
			return true;
		}
		String targetName = canOthers ? (args.length > 2 ? args[2]:null): null;
		Player target = getTarget(sender, targetName, DP);
		if (target == null) { // only if targetName is null or cannot be found AND sender is CONSOLE
			if (targetName == null) { // CONSOLE didn't specify target name
				sender.sendMessage(USAGE_ADMIN);
			} else { // this shouldn't happen - if target wasn't found then it reverts to sender if instance of player
				sender.sendMessage(DP.getSettings().getTargetNotFoundMessage(targetName));
			}
			return true;
		}
		
		// nr
		int nr = 1;
		try {
			nr = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {	} // defaults to 0
		if (nr > DP.getSettings().getVirtualStorageSettings().getNrOfChests(target) || nr <= 0) {
			sender.sendMessage(DP.getSettings().getIncorrectNumberMessage(nr));
			return true;
		}
		final int i = nr - 1;
		handler.getPlayerChests(target).setInventory(i, new ItemStack[0]);
		String msg;
		if (target == player) {
			msg = DP.getSettings().getClearingVirtualStorageMessage(nr);
		} else {
			msg = DP.getSettings().getClearingVirtualStorageOtherMessage(target.getName(), nr);
		}
		sender.sendMessage(msg);
		return true;
	}

	@Override
	public String getUsage(boolean admin) {
		return admin ? USAGE_ADMIN : USAGE;
	}

}
