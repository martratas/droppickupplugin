package me.ford.droppickup.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class HelpCommand extends SubCommand {
	private final Collection<String> usageList = new ArrayList<>();
	
	public HelpCommand(Collection<SubCommand> list) {
		for (SubCommand cmd : list) {
			usageList.add(cmd.getUsage());
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		return new ArrayList<>();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		for (String usage : usageList) {
			sender.sendMessage(usage);
		}
		return true;
	}

	@Override
	public String getUsage(boolean admin) {
		return "";
	}

}
