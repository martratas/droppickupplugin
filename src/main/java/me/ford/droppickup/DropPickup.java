package me.ford.droppickup;

import org.bukkit.plugin.java.JavaPlugin;

import me.ford.droppickup.Settings.LeftOverAction;
import me.ford.droppickup.commands.AdminCommand;
import me.ford.droppickup.commands.BaseCommand;
import me.ford.droppickup.leftovers.PerPlayerInventoryHandler;
import me.ford.droppickup.listeners.PickupListener;
import me.ford.droppickup.listeners.PlayerListener;
import me.ford.droppickup.listeners.VirtualChestListener;
import me.ford.droppickup.profiles.ProfileHandler;

public class DropPickup extends JavaPlugin {
	private Settings settings;
	private ProfileHandler profiles;
	private PerPlayerInventoryHandler invHandler = null;
	private VirtualChestListener virtChestListener;
	
	@Override
	public void onEnable() {
		saveDefaultConfig();
		getConfig().options().copyDefaults(true);
		saveConfig();
		settings = new Settings(this);
		getServer().getPluginManager().registerEvents(new PickupListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		virtChestListener = new VirtualChestListener();
		getServer().getPluginManager().registerEvents(virtChestListener, this);
		profiles = new ProfileHandler(this);
		
		// commands
		getCommand("droppickup").setExecutor(new BaseCommand(this));
		getCommand("droppickupadmin").setExecutor(new AdminCommand(this));
		
		// if using virtual storage
		if (settings.getActionOnLeftOver() == LeftOverAction.VIRTUALSTORE) {
			invHandler = new PerPlayerInventoryHandler(this);
		}
	}
	
	public void reload() {
		reloadConfig();
		profiles.reload();
		if (settings.getActionOnLeftOver() == LeftOverAction.VIRTUALSTORE) {
			invHandler = new PerPlayerInventoryHandler(this);
		} else {
			invHandler = null;
		}
	}
	
	public Settings getSettings() {
		return settings;
	}
	
	public ProfileHandler getProfileHandler() {
		return profiles;
	}
	
	public PerPlayerInventoryHandler getInventoryHandler() {
		return invHandler;
	}
	
	public VirtualChestListener getVirtualChestListener() {
		return virtChestListener;
	}

}
