package me.ford.droppickup.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Container;
import org.bukkit.block.ShulkerBox;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Hanging;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.LeashHitch;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Painting;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.minecart.HopperMinecart;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.projectiles.ProjectileSource;

import me.ford.droppickup.CooldownHandler;
import me.ford.droppickup.DropPickup;
import me.ford.droppickup.profiles.Profile;
import me.ford.droppickup.utils.EssentialsRipXPFix;

public class PickupListener implements Listener {
	private final DropPickup DP;
	private final CooldownHandler invFullCooldown;
	private final ToolDropHandler handler;
	
	public PickupListener(DropPickup plugin) {
		DP = plugin;
		handler = new ToolDropHandler(DP);
		invFullCooldown = new CooldownHandler(DP.getSettings().fullMessageCooldown());
	}
	
	@EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (!player.hasPermission("droppickup.pickup.break")) return;
		if (player.getGameMode() != GameMode.SURVIVAL) return; // TODO - configurable?

		
		ItemStack tool = player.getInventory().getItemInMainHand();
		Block block = event.getBlock();
		player.incrementStatistic(Statistic.MINE_BLOCK, block.getType());
		if (handleSignsAndBanners(block)) return;
		Collection<ItemStack> drops = handler.getDrops(block, tool); // block.getDrops(tool);
		drops.remove(new ItemStack(Material.AIR)); // remove air // TODO - fix WHERE is it coming from?
		handleSugarCaneAndCactus(block, drops);
		magnet(drops, player, block.getLocation(), false);
		
		// for containers
		BlockState state = block.getState();
		if (state instanceof Container && player.hasPermission("droppickup.pickup.break.inventory")) {
			List<ItemStack> contents = new ArrayList<>();
			if (state instanceof ShulkerBox) {
				DP.getLogger().info("SB: " + block.getDrops());
				ItemStack box = new ItemStack(block.getType());
				BlockStateMeta meta = (BlockStateMeta) box.getItemMeta();
				meta.setBlockState(state);
				box.setItemMeta(meta);
				contents.add(box);
			} else {
				Inventory inv = ((Container) state).getInventory();
				for (ItemStack item : inv.getContents()) {
					if (item != null && item.getType() != Material.AIR) {
						contents.add(item);
					}
				}
				inv.clear();
			}
			magnet(contents, player, block.getLocation(), false);
		}
		event.setDropItems(false);
		// handle XP
		if (!DP.getSettings().getGatherXP()) return;

		int xp = event.getExpToDrop();
		if (xp > 0) EssentialsRipXPFix.setTotalExperience(player, EssentialsRipXPFix.getTotalExperience(player) + xp);
		event.setExpToDrop(0);
	}

	private boolean handleSignsAndBanners(Block block) {
		BlockData bData = block.getBlockData();
		if (bData instanceof WallSign || bData instanceof Sign) return true;
		if (block.getState() instanceof Banner) return true;
		return false;
	}

	private void handleSugarCaneAndCactus(Block block, Collection<ItemStack> drops) {
		Material blockType = block.getType();
		if (blockType != Material.SUGAR_CANE && blockType != Material.CACTUS) return;
		int amount = handleOneSugarCaneOrCactus(block, drops, blockType);
		if (amount > 1) drops.add(new ItemStack(blockType, amount - 1));
	}

	private int handleOneSugarCaneOrCactus(Block block, Collection<ItemStack> drops, Material blockType) {
		if (block.getType() == blockType) {
			int above = handleOneSugarCaneOrCactus(block.getRelative(BlockFace.UP), drops, blockType);
			// drops.add(new ItemStack(blockType));
			block.setType(Material.AIR);
			return 1 + above;
		}
		return 0;
	}

	private final Map<TreeSpecies, Material> boatMap = new EnumMap<>(TreeSpecies.class);
	{
		boatMap.put(TreeSpecies.ACACIA, Material.ACACIA_BOAT);
		boatMap.put(TreeSpecies.BIRCH, Material.BIRCH_BOAT);
		boatMap.put(TreeSpecies.DARK_OAK, Material.DARK_OAK_BOAT);
		boatMap.put(TreeSpecies.GENERIC, Material.OAK_BOAT);
		boatMap.put(TreeSpecies.JUNGLE, Material.JUNGLE_BOAT);
		boatMap.put(TreeSpecies.REDWOOD, Material.SPRUCE_BOAT);
	}

	private final Map<EntityType, List<Material>> minecartMap = new EnumMap<>(EntityType.class);
	{
		minecartMap.put(EntityType.MINECART, Lists.newArrayList(Material.MINECART));
		minecartMap.put(EntityType.MINECART_TNT, Lists.newArrayList(Material.MINECART, Material.TNT));
		// Leaving these out: EntityType.MINECART_COMMAND, EntityType.MINECART_MOB_SPAWNER
	}

	@EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
	public void onVehicleDestroy(VehicleDestroyEvent event) {
		Entity damager = event.getAttacker();
		if (damager instanceof Projectile) {
			ProjectileSource source = ((Projectile) damager).getShooter();
			if (source instanceof Entity) {
				damager = (Entity) source;
			}
		}
		if (!(damager instanceof Player)) {
			return;
		}
		// TODO - perms?
		Player player = (Player) damager;
		if (player.getGameMode() != GameMode.SURVIVAL) return; // TODO - configurable?
		DP.getLogger().info("Destroyed:" + event.getVehicle() + " by : " + player);
		Vehicle vehicle = event.getVehicle();
		if (vehicle instanceof Boat) {
			Boat boat = (Boat) vehicle;
			TreeSpecies boatType = boat.getWoodType();
			Material mat = boatMap.get(boatType);
			magnet(Lists.newArrayList(new ItemStack(mat)), player, boat.getLocation(), false);
			event.setCancelled(true);
			boat.remove(); // no drops I hope
			return;
		}
		if (vehicle instanceof Minecart) {
			boolean storageCart = vehicle instanceof StorageMinecart;
			boolean hopperCart = vehicle instanceof HopperMinecart;
			List<ItemStack> contents = new ArrayList<>();
			if (storageCart || hopperCart) {
				Inventory inv;
				if (storageCart) {
					StorageMinecart cart = (StorageMinecart) vehicle;
					inv = cart.getInventory();
				} else {
					HopperMinecart cart = (HopperMinecart) vehicle;
					inv = cart.getInventory();
				}
				contents.add(new ItemStack(Material.MINECART));
				contents.add(new ItemStack(Material.CHEST));
				for (ItemStack item : inv.getContents()) {
					if (item != null && item.getType() != Material.AIR) {
						contents.add(item);
					}
				}
				inv.clear();
			} else {
				List<Material> drops = minecartMap.get(vehicle.getType());
				if (drops != null) {
					for (Material mat : drops) {
						contents.add(new ItemStack(mat));
					}
				}
			}
			magnet(contents, player, vehicle.getLocation(), false);
			event.setCancelled(true);
			vehicle.remove(); // no drops I hope
			return;
		}
	}

	@EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
	public void onHangingBreak(HangingBreakByEntityEvent event) { // TODO - perms?
		Entity damager = event.getRemover();
		if (damager instanceof Projectile) {
			ProjectileSource source = ((Projectile) damager).getShooter();
			if (source instanceof Entity) {
				damager = (Entity) source;
			}
		}
		if (!(damager instanceof Player)) {
			return;
		}
		Player player = (Player) damager;
		if (player.getGameMode() != GameMode.SURVIVAL) return; // TODO - configurable?
		Hanging entity = event.getEntity();
		if (entity instanceof ItemFrame) {
			ItemFrame frame = (ItemFrame) entity;
			List<ItemStack> items = new ArrayList<>();
			items.add(new ItemStack(Material.ITEM_FRAME));
			ItemStack itemInFrame = frame.getItem();
			if (itemInFrame.getType() != Material.AIR) items.add(itemInFrame);
			magnet(items, player, frame.getLocation(), false);
			event.setCancelled(true);
			frame.remove();
		}
		if (entity instanceof Painting) {
			magnet(Lists.newArrayList(new ItemStack(Material.PAINTING)), player, entity.getLocation(), false);
			event.setCancelled(true);
			entity.remove();
		}
		if (entity instanceof LeashHitch) {
			magnet(Lists.newArrayList(new ItemStack(Material.LEAD)), player, entity.getLocation(), false);
			event.setCancelled(true);
			entity.remove();
			removeNextSpawn = true;
		}
	}

	private boolean removeNextSpawn = false;

	@EventHandler
	public void onUnleash(ItemSpawnEvent event) {
		if (!removeNextSpawn) return;
		event.setCancelled(true);
		removeNextSpawn = false;
	}

	@EventHandler(priority=EventPriority.HIGHEST, ignoreCancelled=true)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		Entity damager = event.getDamager();
		if (damager instanceof Projectile) {
			ProjectileSource source = ((Projectile) damager).getShooter();
			if (source instanceof Entity) {
				damager = (Entity) source;
			}
		}
		if (!(damager instanceof Player)) {
			return;
		}
		Player player = (Player) damager;
		if (player.getGameMode() != GameMode.SURVIVAL) return; // TODO - configurable?
		Entity entity = event.getEntity();
		if (entity instanceof ItemFrame) {
			ItemFrame frame = (ItemFrame) entity;
			List<ItemStack> items = new ArrayList<>();
			items.add(frame.getItem());
			magnet(items, player, frame.getLocation(), false);
			event.setCancelled(true);
			frame.setItem(new ItemStack(Material.AIR), true);
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEntityDeath(EntityDeathEvent event) {
		
		Entity damager = getDamager(event);
		if (damager instanceof Projectile) {
			ProjectileSource source = ((Projectile) damager).getShooter();
			if (source instanceof Entity) {
				damager = (Entity) source;
			}
		}
		if (!(damager instanceof Player)) {
			return;
		}
		Player player = (Player) damager;
		if (player.getGameMode() != GameMode.SURVIVAL) return; // TODO - configurable?
		if (!player.hasPermission("droppickup.pickup.kill")) return;
		
		List<ItemStack> contents = event.getDrops();
		
		LivingEntity entity = event.getEntity();
		if (entity.isLeashed()) {
			Entity holder = entity.getLeashHolder();
			if (holder instanceof LeashHitch) {
				holder.remove();
				contents.add(new ItemStack(Material.LEAD));
			} else {
				DP.getLogger().warning("Unexpected leash holder!" + holder);
			}
			entity.setLeashHolder(null);
		}
		magnet(contents, player, entity.getLocation(), true);
		// handle XP
		if (!DP.getSettings().getGatherXP()) return;

		int xp = event.getDroppedExp();
		if (xp > 0) EssentialsRipXPFix.setTotalExperience(player, EssentialsRipXPFix.getTotalExperience(player) + xp);
		event.setDroppedExp(0);
	}
	
	private Entity getDamager(EntityDeathEvent event) {
		if (!(event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
			return null;
		}
		EntityDamageByEntityEvent eEvent = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();
		return eEvent.getDamager();
	}
	
	private boolean magnet(Collection<ItemStack> drops, Player player, Location loc, boolean dropInCollection) {
		List<ItemStack> toPickUp = getItemsToPickUp(drops, DP.getProfileHandler().getPlayerProfile(player));
		Map<Integer, ItemStack> leftOvers = new HashMap<>();
		int i = 1;
		for (ItemStack drop : drops) { // those the player chose not to pick up by profile
			leftOvers.put(-i, drop);
			i++;
		}
		// and those that don't fit below
		Map<Integer, ItemStack> noFit = player.getInventory().addItem(toPickUp.toArray(new ItemStack[0]));
		if (!noFit.isEmpty() && DP.getSettings().sendMessageWhenFull() && !invFullCooldown.isOnCooldown(player)) {
			player.sendMessage(DP.getSettings().getInventoryFullMessage());
			invFullCooldown.startCooldown(player, DP.getSettings().fullMessageCooldown()); // in case of reloads
		}
		leftOvers.putAll(noFit);
		drops.clear(); // will repopulate as needed
		if (leftOvers.isEmpty()) {
			return true;
		}
		
		switch(DP.getSettings().getActionOnLeftOver()) {
		case DROP:
			if (!dropInCollection) {
				for (ItemStack item : leftOvers.values()) {
					loc.getWorld().dropItemNaturally(loc, item);
				}
			} else {
				drops.addAll(leftOvers.values());
			}
			break;
		case VIRTUALSTORE:
			List<ItemStack> finaldrops = new ArrayList<>(leftOvers.values());
			DP.getInventoryHandler().addItems(player, finaldrops);
			if (!finaldrops.isEmpty()) {
				if (dropInCollection) {
					drops.addAll(finaldrops);
				} else {
					for (ItemStack item : finaldrops) {
						loc.getWorld().dropItemNaturally(loc, item);
					}
				}
			}
			break;
		case STORE:
			throw new UnsupportedOperationException("Not implemented yet!");
		case DESTROY:
		default:
			break; // NOTHING - it'll be destroyed
		}
		return false;
	}
	
	private List<ItemStack> getItemsToPickUp(Collection<ItemStack> drops, Profile profile) {
		List<ItemStack> toPickUp = new ArrayList<>();
		for (ItemStack drop : new ArrayList<>(drops)) {
			if (profile.canPickup(drop.getType())) {
				toPickUp.add(drop);
				drops.remove(drop);
			}
		}
		return toPickUp;
	}

}
