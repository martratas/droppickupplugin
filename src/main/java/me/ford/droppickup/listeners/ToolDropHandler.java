package me.ford.droppickup.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import com.google.common.collect.Lists;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.ShulkerBox;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.type.Bed;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.block.data.type.TrapDoor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import me.ford.droppickup.DropPickup;

/**
 * ToolHandler
 */
public class ToolDropHandler {
	private final Set<Material> silkables = EnumSet.of(Material.COAL_ORE, Material.DIAMOND_ORE, Material.LAPIS_ORE, Material.REDSTONE_ORE, Material.EMERALD_ORE,
								Material.BLUE_ICE, Material.PACKED_ICE, Material.ICE,
								Material.BOOKSHELF, Material.ENDER_CHEST,
								Material.CLAY,
								Material.BRAIN_CORAL, Material.BRAIN_CORAL_BLOCK, Material.BRAIN_CORAL_FAN, Material.BRAIN_CORAL_WALL_FAN,
								Material.BUBBLE_CORAL, Material.BUBBLE_CORAL_BLOCK, Material.BUBBLE_CORAL_FAN, Material.BUBBLE_CORAL_WALL_FAN,
								Material.FIRE_CORAL, Material.FIRE_CORAL_BLOCK, Material.FIRE_CORAL_FAN, Material.FIRE_CORAL_WALL_FAN,
								Material.HORN_CORAL, Material.HORN_CORAL_BLOCK, Material.HORN_CORAL_FAN, Material.HORN_CORAL_WALL_FAN,
                                Material.TUBE_CORAL, Material.TUBE_CORAL_BLOCK, Material.TUBE_CORAL_FAN, Material.TUBE_CORAL_WALL_FAN,
                                Material.GLASS,
                                Material.BLACK_STAINED_GLASS, Material.BLUE_STAINED_GLASS, Material.BROWN_STAINED_GLASS, Material.CYAN_STAINED_GLASS,
                                Material.GRAY_STAINED_GLASS, Material.GREEN_STAINED_GLASS, Material.LIGHT_BLUE_STAINED_GLASS, Material.LIGHT_GRAY_STAINED_GLASS,
                                Material.LIME_STAINED_GLASS, Material.MAGENTA_STAINED_GLASS, Material.ORANGE_STAINED_GLASS, Material.PINK_STAINED_GLASS, 
                                Material.PURPLE_STAINED_GLASS, Material.RED_STAINED_GLASS, Material.WHITE_STAINED_GLASS, Material.YELLOW_STAINED_GLASS,
                                Material.BLACK_STAINED_GLASS_PANE, Material.BLUE_STAINED_GLASS_PANE, Material.BROWN_STAINED_GLASS_PANE, Material.CYAN_STAINED_GLASS_PANE,
                                Material.GRAY_STAINED_GLASS_PANE, Material.GREEN_STAINED_GLASS_PANE, Material.LIGHT_BLUE_STAINED_GLASS_PANE, Material.LIGHT_GRAY_STAINED_GLASS_PANE,
                                Material.LIME_STAINED_GLASS_PANE, Material.MAGENTA_STAINED_GLASS_PANE, Material.ORANGE_STAINED_GLASS_PANE, Material.PINK_STAINED_GLASS_PANE, 
                                Material.PURPLE_STAINED_GLASS_PANE, Material.RED_STAINED_GLASS_PANE, Material.WHITE_STAINED_GLASS_PANE, Material.YELLOW_STAINED_GLASS_PANE,
                                Material.GLOWSTONE, Material.GRASS_BLOCK, Material.GRAVEL,
                                Material.OAK_LEAVES, Material.DARK_OAK_LEAVES, Material.BIRCH_LEAVES, Material.JUNGLE_LEAVES, Material.ACACIA_LEAVES, Material.SPRUCE_LEAVES,
                                Material.MELON, Material.MUSHROOM_STEM, Material.BROWN_MUSHROOM_BLOCK, Material.RED_MUSHROOM_BLOCK, Material.MYCELIUM,
                                Material.NETHER_QUARTZ_ORE, Material.PODZOL,
                                Material.SEA_LANTERN, Material.SNOW_BLOCK, Material.SNOW, Material.STONE, Material.TURTLE_EGG
                                );
    private final Set<Material> pickaxeSilkables = EnumSet.of(Material.COAL_ORE, Material.DIAMOND_ORE, Material.LAPIS_ORE, Material.REDSTONE_ORE, 
                                Material.EMERALD_ORE, Material.NETHER_QUARTZ_ORE,
                                Material.ENDER_CHEST, Material.STONE, 
								Material.BRAIN_CORAL, Material.BRAIN_CORAL_BLOCK, Material.BRAIN_CORAL_FAN, Material.BRAIN_CORAL_WALL_FAN,
								Material.BUBBLE_CORAL, Material.BUBBLE_CORAL_BLOCK, Material.BUBBLE_CORAL_FAN, Material.BUBBLE_CORAL_WALL_FAN,
								Material.FIRE_CORAL, Material.FIRE_CORAL_BLOCK, Material.FIRE_CORAL_FAN, Material.FIRE_CORAL_WALL_FAN,
								Material.HORN_CORAL, Material.HORN_CORAL_BLOCK, Material.HORN_CORAL_FAN, Material.HORN_CORAL_WALL_FAN,
                                Material.TUBE_CORAL, Material.TUBE_CORAL_BLOCK, Material.TUBE_CORAL_FAN, Material.TUBE_CORAL_WALL_FAN);
    private final Set<Material> shovelSilktables = EnumSet.of(Material.SNOW, Material.SNOW_BLOCK);
    private final Set<Material> ironOrBetterSilkables = EnumSet.of(Material.DIAMOND_ORE, Material.EMERALD_ORE, Material.REDSTONE_ORE);
    private final Material stoneOrBetter = Material.LAPIS_ORE;
    private final Set<Material> pickaxes = EnumSet.of(Material.WOODEN_PICKAXE, Material.STONE_PICKAXE, Material.IRON_PICKAXE, 
                                Material.GOLDEN_PICKAXE, Material.DIAMOND_PICKAXE);
    private final Set<Material> shovels = EnumSet.of(Material.WOODEN_SHOVEL, Material.STONE_SHOVEL, Material.IRON_SHOVEL, 
                                Material.GOLDEN_SHOVEL, Material.DIAMOND_SHOVEL);
    private final Map<Material, Material> fortunate1 = new EnumMap<>(Material.class);
    private final Set<Material> leaves = EnumSet.of(Material.OAK_LEAVES, Material.DARK_OAK_LEAVES, Material.BIRCH_LEAVES, Material.JUNGLE_LEAVES, Material.ACACIA_LEAVES, Material.SPRUCE_LEAVES);
    { //For coal, diamond, emerald, nether quartz, and lapis lazuli,
        // Fortune I gives a 33% chance to multiply drops by 2 (averaging 33% increase), 
        // Fortune II gives a chance to multiply drops by 2 or 3 (25% chance each, averaging 75% increase), 
        // and Fortune III gives a chance to multiply drops by 2, 3, or 4 (20% chance each, averaging 120% increase). 
        // 1 drop has a weight of 2, and each number of extra drops has a weight of 1.
        fortunate1.put(Material.COAL_ORE, Material.COAL);
        fortunate1.put(Material.DIAMOND_ORE, Material.DIAMOND);
        fortunate1.put(Material.EMERALD_ORE, Material.EMERALD);
        fortunate1.put(Material.NETHER_QUARTZ_ORE, Material.QUARTZ);
        fortunate1.put(Material.LAPIS_ORE, Material.LAPIS_LAZULI);
    }
    private final Map<Material, Material> fortunate2 = new EnumMap<>(Material.class);
    { //For redstone, carrots, glowstone, sea lanterns, melons, nether wart, potatoes, beetroots (seeds only) and wheat (seeds only),
        // each level increases the drop maximum by +1 (maximum 4 for glowstone, 5 for sea lanterns, and 9 for melon slices).
        fortunate2.put(Material.CARROTS, Material.CARROT);
        fortunate2.put(Material.GLOWSTONE, Material.GLOWSTONE_DUST);
        fortunate2.put(Material.SEA_LANTERN, Material.PRISMARINE_SHARD);
        fortunate2.put(Material.MELON, Material.MELON_SLICE);
        fortunate2.put(Material.NETHER_WART, Material.NETHER_WART);
        fortunate2.put(Material.POTATOES, Material.POTATO);
        fortunate2.put(Material.BEETROOTS, Material.BEETROOT_SEEDS); // NEEDS EXTRA BEETROOT
        fortunate2.put(Material.WHEAT, Material.WHEAT_SEEDS); // NEEDS EXTRA WHEAT
    }
    // NOT IMPLEMENTED:
     // For tall grass, each level increases the drop maximum by +2.
     // Fortune increases the probability of flint dropping from gravel, and saplings and sticks dropping from leaves, 
    private final Map<Material, Material> shovelFortunate = new EnumMap<>(Material.class);
    {
        shovelFortunate.put(Material.GRAVEL, Material.FLINT);
    }
     // and apples dropping from oak and dark oak leaves.
    private final DropPickup DP;
    private final boolean is113;
    private final boolean is114;

    public ToolDropHandler(DropPickup plugin) {
        DP = plugin;
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        is113 = version.contains("1_13");
        is114 = version.contains("1_14");
    }

    public Collection<ItemStack> getDrops(Block block, ItemStack tool) {
        Material blockMat = block.getType();
        if (block.getState() instanceof ShulkerBox) return new ArrayList<>();
        boolean silk = useSilk(tool, blockMat);
        boolean fortune = useFortune(tool, blockMat);
        if (block.getBlockData() instanceof Bed) {
            return Lists.newArrayList(new ItemStack(blockMat));
        }
        if (block.getBlockData() instanceof Door) {
            return Lists.newArrayList(new ItemStack(blockMat));
        }
        if (blockMat.name().startsWith("POTTED_")) {
            return Lists.newArrayList(new ItemStack(Material.FLOWER_POT), new ItemStack(Material.valueOf(blockMat.name().replace("POTTED_", ""))));
        }
        if (leaves.contains(blockMat) && tool.getType() == Material.SHEARS) {
            return Lists.newArrayList(new ItemStack(blockMat));
        }
        if (is114 || (is113 && !silk && !fortune) ) { // is 1.14+ OR 1.13 AND not silk touch nor fortune
            if (is113 && blockMat == Material.COCOA) {
                Ageable age = (Ageable) block.getBlockData();
                return Lists.newArrayList(new ItemStack(Material.COCOA_BEANS, (age.getAge() < age.getMaximumAge()?1:3)));
            }
            BlockData bData = block.getBlockData();
            if (bData instanceof Bisected && !(bData instanceof Door) && !(bData instanceof TrapDoor) && !(bData instanceof Stairs)) {
                Bisected bisected = (Bisected) bData;
                if (bisected.getHalf() == Half.BOTTOM) {
                    block.setType(Material.AIR);
                    block.getRelative(BlockFace.UP).setType(Material.AIR);
                } else {
                    block.getRelative(BlockFace.DOWN).setType(Material.AIR);
                }
                if (blockMat == Material.TALL_GRASS || blockMat == Material.LARGE_FERN) {
                    if (ThreadLocalRandom.current().nextBoolean()) {
                        return Lists.newArrayList(new ItemStack(Material.WHEAT_SEEDS));
                    } else {
                        return Lists.newArrayList();
                    }
                } else {
                    return Lists.newArrayList(new ItemStack(blockMat));
                }
            }
            if (blockMat == Material.CHORUS_FLOWER) {
                return Lists.newArrayList(new ItemStack(blockMat));
            }
            if (blockMat == Material.CHORUS_PLANT) { // TODO - what about the top ones?
                return Lists.newArrayList(new ItemStack(Material.CHORUS_FRUIT));
            }
            return block.getDrops(tool);
        }
        if (!is114 && !is113) {
            DP.getLogger().warning("Unsupported MC version! Drops (especially from Silk Touch and Fortune) are likely to be incorrect!");
            return block.getDrops(tool);
        }
        List<ItemStack> items = new ArrayList<>();
        if (silk) {
            items.add(new ItemStack(blockMat));
            return items; // TODO - is this correct? 
        } else if (fortune) {
            int lvl = tool.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
            Material shovelGive = shovelFortunate.get(blockMat);
            if (shovelGive != null) {
                double chance;
                switch(lvl) {
                    case 0:
                    chance = .1;
                    break;
                    case 1:
                    chance = .14;
                    break;
                    case 2:
                    chance = .25;
                    break;
                    case 3:
                    default:
                    chance = 1;
                }
                if (ThreadLocalRandom.current().nextDouble() < chance) {
                    items.add(new ItemStack(shovelGive));
                } else {
                    items.add(new ItemStack(blockMat));
                }
                return items;
            }
            if (blockMat == Material.REDSTONE_ORE) {
                items.add(handleRedstoneFortune(lvl));
                return items;
            }
            Material give = fortunate1.get(blockMat);
            if (give != null) {
                for (ItemStack drop : block.getDrops(tool)) {
                    if (drop.getType() == give) {
                        items.add(handleFortune1(give, lvl));
                    } else {
                        DP.getLogger().info("Different types with fortune while mining " + blockMat + " -> " + give + " vs " + drop.getType() + " lvl:" + lvl);
                        DP.getLogger().warning("The above has not been taken into account - alert the developer!");
                    }
                }
                return items;
            }
            give = fortunate2.get(blockMat);
            if (give != null) {
                BlockData bData = block.getBlockData();
                if (bData instanceof Ageable) {
                    Ageable age = (Ageable) bData;
                    if (age.getAge() < age.getMaximumAge()) { // NOT fully grown -> 1 "seed"
                        items.add(new ItemStack(fortunate2.get(blockMat))); // just the 1 seed!
                        return items;
                    }
                }
                items.add(handleFortune2(give, lvl));
                if (give == Material.BEETROOT_SEEDS) {
                    items.add(new ItemStack(Material.BEETROOT));
                }
                if (give == Material.WHEAT_SEEDS) {
                    items.add(new ItemStack(Material.WHEAT));
                }
                return items;
            }
            items.addAll(block.getDrops(tool)); // in everyday simple items (since this also gets called for fortune 0)
            return items;
        }
        DP.getLogger().warning("Questionable behavior when calculating drops. Silk?" + silk + " fortune?" + fortune + " 1.14?" + is114 + " 1.13?" + is113);
        return items;
    }

    private ItemStack handleRedstoneFortune(int lvl) {
        ItemStack item = new ItemStack(Material.REDSTONE);
        int amount;
        double rnd = ThreadLocalRandom.current().nextDouble();
        double cur = 1/(2 * (1. + lvl));
        amount = 4;
        while (rnd > cur) {
            cur += 1/(1. + lvl);
            amount++;
        }
        item.setAmount(amount);
        return item;
    }
 //For coal, diamond, emerald, nether quartz, and lapis lazuli,
        // Fortune I gives a 33% chance to multiply drops by 2 (averaging 33% increase), 
        // Fortune II gives a chance to multiply drops by 2 or 3 (25% chance each, averaging 75% increase), 
        // and Fortune III gives a chance to multiply drops by 2, 3, or 4 (20% chance each, averaging 120% increase). 
        // 1 drop has a weight of 2, and each number of extra drops has a weight of 1.
    private ItemStack handleFortune1(Material give, int lvl) {
        double rnd = ThreadLocalRandom.current().nextDouble();
        ItemStack item = new ItemStack(give);
        double cur = 2./(lvl + 2);
        if (rnd < cur) { // 1x
            return item;
        }
        double increment = 1./(lvl + 2);
        for (int mult = 2; mult <= lvl + 1; mult++) {
            cur += increment;
            if (rnd < cur) {
                item.setAmount(mult);
                return item;
            }
        }
        DP.getLogger().warning("Fortune misfunction (type 1)! " + rnd + " at lvl " + lvl);
        return item;
    }
 //For redstone, carrots, glowstone, sea lanterns, melons, nether wart, potatoes, sweet berries, beetroots (seeds only) and wheat (seeds only),
    // each level increases the drop maximum by +1 (maximum 4 for glowstone, 5 for sea lanterns, and 9 for melon slices).
    private ItemStack handleFortune2(Material give, int lvl) {
        ItemStack item = new ItemStack(give);
        int minAmount = 1;
        int maxAmount;
        switch (give) {
            case CARROT:
                maxAmount = 5;
                break;
            case GLOWSTONE_DUST:
            case NETHER_WART:
                minAmount = 2;
                maxAmount = 4;
                break;
            case PRISMARINE_SHARD:
                minAmount = 2;
                maxAmount = 3;
                break;
            case MELON_SLICE:
                minAmount = 3;
                maxAmount = 7;
                break;
            case POTATO:
                maxAmount = 4;
                break;
            case BEETROOT_SEEDS:
            case WHEAT_SEEDS:
                minAmount = 0;
                maxAmount = 3;
                break;
            default:
                maxAmount = 1;
                break;
        }
        maxAmount += lvl;
        if (give == Material.GLOWSTONE_DUST && maxAmount > 4) maxAmount = 4;
        if (give == Material.PRISMARINE_SHARD && maxAmount > 5) maxAmount = 5;
        if (give == Material.MELON_SLICE && maxAmount > 9) maxAmount = 9; 
        int amount = ThreadLocalRandom.current().nextInt(maxAmount - minAmount + 1) + minAmount;
        item.setAmount(amount);
        return item;
    }

    public boolean useSilk(ItemStack tool, Material mat) {
        if (tool == null) return false;
        boolean silkable = tool.containsEnchantment(Enchantment.SILK_TOUCH) && silkables.contains(mat);
        if (!silkable) return false;
        if (pickaxeSilkables.contains(mat)) {
            Material toolType = tool.getType();
            if (!isPickaxe(toolType)) return false;
            if (ironOrBetterSilkables.contains(mat)) {
                switch(toolType) {
                    case IRON_PICKAXE:
                    case DIAMOND_PICKAXE:
                    return true;
                    default:
                    return false;
                }
            }
            if (mat == stoneOrBetter) {
                switch(toolType) {
                    case STONE_PICKAXE:
                    case IRON_PICKAXE:
                    case DIAMOND_PICKAXE:
                    return true;
                    default:
                    return false;
                }
            }
            return true; // any pickaxe
        }
        if (shovelSilktables.contains(mat)) {
            return isShovel(tool.getType());
        }
        return silkable;
    }

    public boolean isPickaxe(Material mat) {
        return pickaxes.contains(mat);
    }

    public boolean isShovel(Material mat) {
        return shovels.contains(mat);
    }

    public boolean useFortune(ItemStack tool, Material mat) {
        if (tool == null) return false;
        return !tool.containsEnchantment(Enchantment.SILK_TOUCH) && (fortunate1.containsKey(mat) || fortunate2.containsKey(mat) || shovelFortunate.containsKey(mat) || mat == Material.REDSTONE_ORE);
    }
    
}