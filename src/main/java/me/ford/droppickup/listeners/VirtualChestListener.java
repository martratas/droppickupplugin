package me.ford.droppickup.listeners;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;

//import me.ford.droppickup.DropPickup;

public class VirtualChestListener implements Listener {
//	private final DropPickup DP;
	private static final Set<InventoryAction> CLICKS = EnumSet.of(InventoryAction.HOTBAR_MOVE_AND_READD, InventoryAction.HOTBAR_SWAP,
									InventoryAction.PLACE_ALL, InventoryAction.PLACE_ONE, InventoryAction.PLACE_SOME, InventoryAction.SWAP_WITH_CURSOR);
	private static final Set<InventoryAction> CLICKS_OTHER = EnumSet.of(InventoryAction.MOVE_TO_OTHER_INVENTORY);
	private final Map<Inventory, Runnable> listenTo = new HashMap<>();
	
	public VirtualChestListener(/*DropPickup plugin*/) {
//		DP = plugin;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Inventory top = event.getView().getTopInventory();
		if (!listenTo.containsKey(top)) {
			return;
		}
		if (top == event.getClickedInventory() && CLICKS.contains(event.getAction())) {
			event.setCancelled(true);
		} else if (top != event.getClickedInventory() && CLICKS_OTHER.contains(event.getAction())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler 
	public void onInventoryDrag(InventoryDragEvent event) {
		Inventory top = event.getView().getTopInventory();
		if (!listenTo.containsKey(top)) {
			return;
		}
		Set<Integer> slots = event.getRawSlots();
		boolean inTop = false;
		for (int i : slots) {
			if (i < top.getSize()) {
				inTop = true;
				break;
			}
		}
		if (inTop) event.setCancelled(true);
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		Inventory inv = event.getInventory();
		if (listenTo.containsKey(inv)) {
			stopListen(inv);
			
		}
	}
	
	public void startListen(Inventory inv, Runnable run) {
		listenTo.put(inv, run);
	}
	
	public void stopListen(Inventory inv) {
		Runnable run = listenTo.remove(inv);
		if (run != null) run.run();
	}

}
