package me.ford.droppickup.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import me.ford.droppickup.DropPickup;
import me.ford.droppickup.leftovers.PerPlayerInventoryHandler;

public class PlayerListener implements Listener {
	private final DropPickup DP;
	
	public PlayerListener(DropPickup plugin) {
		DP = plugin;
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		DP.getProfileHandler().playerLeft(player);
		PerPlayerInventoryHandler handler = DP.getInventoryHandler();
		if (handler != null) handler.playerLeft(player);
	}

}
