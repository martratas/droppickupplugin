package me.ford.droppickup.profiles;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.OfflinePlayer;

import me.ford.droppickup.DropPickup;
import me.ford.droppickup.utils.CustomConfig;

public class ProfileHandler extends CustomConfig {
	private static final String FILE_NAME = "profiles.yml";
	private final DropPickup DP;
	private final Map<String, Profile> profiles = new HashMap<>(); 
	private final PlayerProfileHandler playerProfileConfig;
	
	public ProfileHandler(DropPickup plugin) {
		super(plugin, FILE_NAME);
		playerProfileConfig = new PlayerProfileHandler(plugin, this);
		getConfig().options().copyDefaults(true);
		saveConfig();
		DP = plugin;
	}
	
	@Override
	public void reload() {
		super.reload();
		for (Profile profile : profiles.values()) {
			profile.reload(getConfig()); // they remember their names
		}
		playerProfileConfig.reload();
	}
	
	public Profile getProfile(String name) {
		if (name == null) return null;
		Profile profile = getCached(name);
		if (profile != null) return profile;
		if (isProfile(name)) {
			profile = new Profile(getConfig().getConfigurationSection(name));
			addToCache(name, profile);
			return profile;
		} else {
			return null;
		}
	}
	
	public Set<String> getProfileNames() {
		return getConfig().getKeys(false);
	}
	
	public boolean isProfile(String profile) {
		return isCached(profile) || getConfig().isConfigurationSection(profile);
	}
	
	public Profile getDefaultProfile() {
		return getProfile(DP.getSettings().getDefaultProfile());
	}
	
	private boolean isCached(String profile) {
		return getCached(profile) != null;
	}
	
	private Profile getCached(String profile) {
		return profiles.get(profile.toLowerCase());
	}
	
	private void addToCache(String name, Profile profile) {
		profiles.put(name.toLowerCase(), profile);
	}
	
	// per player
	
	public Profile getPlayerProfile(OfflinePlayer player) {
		return playerProfileConfig.getProfile(player);
	}
	
	public void setPlayerProfile(OfflinePlayer player, Profile profile) {
		playerProfileConfig.setProfile(player, profile);
	}
	
	public void playerLeft(OfflinePlayer player) {
		playerProfileConfig.loggedOut(player);
	}
	
}
