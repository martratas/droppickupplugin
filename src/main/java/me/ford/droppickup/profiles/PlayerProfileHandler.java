package me.ford.droppickup.profiles;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//import org.apache.commons.lang.Validate;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.droppickup.utils.CustomConfig;

public class PlayerProfileHandler extends CustomConfig {
	private static final String NAME = "player_profiles.yml";
	private final Map<UUID, Profile> playerProfiles = new HashMap<>();
	private final ProfileHandler handler;
	
	public PlayerProfileHandler(JavaPlugin plugin, ProfileHandler handler) {
		super(plugin, NAME);
//		Validate.notNull(handler, "ProfileHandler cannot be null!");
		this.handler = handler;
	}
	
	public Profile getProfile(OfflinePlayer player) {
//		Validate.notNull(player, "Player cannot be null!");
		Profile profile = getCached(player.getUniqueId());
		if (profile == null) {
			profile = handler.getProfile(getConfig().getString(player.getUniqueId().toString(), null));
			if (profile == null) {
				profile = handler.getDefaultProfile(); // not writing it down as long as it's the default one
			}
			addToCache(player.getUniqueId(), profile);
		}
		return profile;
	}
	
	public void setProfile(OfflinePlayer player, Profile profile) {
//		Validate.notNull(player, "Player cannot be null!");
		if (profile == null) profile = handler.getDefaultProfile();
		
		String id = player.getUniqueId().toString();
		if (profile == handler.getDefaultProfile()) { // should be the same instance
			getConfig().set(id, null); // default -> not writing down
		} else {
			getConfig().set(id, profile.getName());
		}
		saveConfig(); // TODO - mark for saving and only save occasionally for optimal performance
		addToCache(player.getUniqueId(), profile);
	}
	
	public void loggedOut(OfflinePlayer player) {
		removeFromCache(player.getUniqueId());
	}
	
	// CACHE
	
	private void addToCache(UUID uuid, Profile profile) {
		playerProfiles.put(uuid, profile);
	}
	
	private Profile getCached(UUID uuid) {
		return playerProfiles.get(uuid);
	}
	
	private void removeFromCache(UUID uuid) { // log out
		playerProfiles.remove(uuid);
	}

}
