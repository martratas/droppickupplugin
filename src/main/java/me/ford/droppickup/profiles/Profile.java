package me.ford.droppickup.profiles;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
//import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.Validate;

public class Profile {
	private static final Set<Material> MATERIALS = EnumSet.allOf(Material.class);
	private ConfigurationSection config; // might get reloaded
	private final Logger logger;
	private final Set<Material> onList = new HashSet<>();
	private final Set<Material> offList = new HashSet<>();
	public boolean isAllowed;
	
	public Profile(ConfigurationSection section) {
//		Validate.notNull(section, "Section cannot be null!");
		config = section;
		logger = Logger.getLogger(config.getName());
		reload(config);
	}
	
	public void reload(ConfigurationSection config) {
		this.config = config; // new instance
		onList.clear();
		onList.addAll(getMaterialsOnList());
		offList.clear();
		offList.addAll(getMaterialsNotOnList());
		isAllowed = getIsAllowed();
	}
	
	private Set<Material> getMaterialsOnList() {
		List<String> list = config.getStringList("material-list");
		Set<Material> set = new HashSet<>();
		for (String matName : list) {
			Material mat = Material.matchMaterial(matName);
			if (mat != null) {
				set.add(mat);
			} else {
				logger.warning("Unable to parse Material from: " + matName);
			}
		}
		return set;
	}
	
	private Set<Material> getMaterialsNotOnList() {
		Set<Material> all = new HashSet<>();
		all.addAll(MATERIALS);
		all.removeAll(getMaterialsOnList());
		return all;
	}
	
	private boolean getIsAllowed() {
		return config.getBoolean("list-allowed");
	}
	
	public boolean isAllowed() {
		return isAllowed;
	}
	
	public String getName() {
		return config.getName();
	}
	
	public Set<Material> materialsOnList() {
		return onList;
	}
	
	public Set<Material> materialsNotOnList() {
		return offList;
	}
	
	public Set<Material> getAllowedMaterials() {
		if (isAllowed) {
			return materialsOnList();
		} else {
			return materialsNotOnList();
		}
	}
	
	public Set<Material> getDisallowedMaterials() {
		if (isAllowed) {
			return materialsNotOnList();
		} else {
			return materialsOnList();
		}
	}
	
	public boolean canPickup(Material mat) {
		return getAllowedMaterials().contains(mat);
	}

	@Override
	public String toString() {
		return getName();
	}

}
