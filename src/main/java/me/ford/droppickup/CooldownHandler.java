package me.ford.droppickup;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Entity;

public class CooldownHandler {
	private final Map<UUID, Long> cooldowns = new HashMap<>();
	private final long defaultCooldown;
	
	public CooldownHandler(long cooldown) {
		defaultCooldown = cooldown;
	}
	
	public void startCooldown(Entity entity) {
		startCooldown(entity.getUniqueId());
	}
	
	public void startCooldown(UUID id) {
		startCooldown(id, defaultCooldown);
	}
	
	public void startCooldown(Entity entity, long seconds) {
		startCooldown(entity.getUniqueId(), seconds);
	}
	
	public void startCooldown(UUID id, long seconds) {
		cooldowns.put(id, System.currentTimeMillis() + seconds * 1000L);
	}
	
	public boolean endCooldown(Entity entity) {
		return endCooldown(entity.getUniqueId());
	}
	
	public boolean endCooldown(UUID id) {
		return cooldowns.remove(id) != null;
	}
	
	public boolean isOnCooldown(Entity entity) {
		return isOnCooldown(entity.getUniqueId());
	}
	
	public boolean isOnCooldown(UUID id) {
		Long end = cooldowns.get(id);
		if (end == null) return false;
		if (end < System.currentTimeMillis()) {
			cooldowns.remove(id);
			return false;
		}
		return true;
		
	}
	
	public long cooldownEnd(Entity entity) {
		Long end = cooldowns.get(entity.getUniqueId());
		if (end == null) return System.currentTimeMillis();
		return end;
	}
	
	public Map<UUID, Long> getCooldowns() {
		Map<UUID, Long> map = new HashMap<>(cooldowns); // clone
		return map;
	}

}
