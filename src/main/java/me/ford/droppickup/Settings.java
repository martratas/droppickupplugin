package me.ford.droppickup;

import org.bukkit.ChatColor;

import me.ford.droppickup.leftovers.VirtualStorageSettings;

public class Settings {
	private final DropPickup DP;
	private final VirtualStorageSettings vsSettings;
	
	public Settings(DropPickup plugin) {
		DP = plugin;
		vsSettings = new VirtualStorageSettings(DP);
	}
	
	public LeftOverAction getActionOnLeftOver() {
		return LeftOverAction.valueOf(DP.getConfig().getString("left-over-action"));
	}

	public boolean getGatherXP() {
		return DP.getConfig().getBoolean("gather-xp", false);
	}
	
	public String getDefaultProfile() {
		return DP.getConfig().getString("default-profile", "DEFAULT");
	}
	
	public boolean sendMessageWhenFull() {
		return DP.getConfig().getBoolean("send-message-when-full", true);
	}
	
	public int fullMessageCooldown() {
		return DP.getConfig().getInt("full-message-cooldown", 10);
	}
	
	public String getNoPermissionMessage() {
		return getMessage("no-permission", "&cYou do not have permission to use this command!");
	}
	
	public String getInventoryFullMessage() {
		return getMessage("inv-full", "&6Your inventory is full - could not automatically pick up items!");
	}

	public String getConfigReloadMessage() {
		return getMessage("config-reload", "&cYou reloaded the config!");
	}
	
	public String getTargetNotFoundMessage(String name) {
		return getMessage("target-not-found", "&cThe player &7{name}&6 was not found!").replace("{name}", name);
	}
	
	public String getProfileNotFoundMessage(String profile) {
		return getMessage("profile-not-found", "&cThe profile &7{profile}&c was not found!").replace("{profile}", profile);
	}
	
	public String getAlreadyOnProfileMessage(String profile) {
		return getMessage("already-on-profile", "&6The profile &7{profile}&6 was already selected").replace("{profile}", profile);
	}
	
	public String getSetProfileFromToMessage(String from, String to) {
		return getMessage("set-profile-from-to", "&6Set the profile from &7{from}&6 to &8{to}&6.").replace("{from}", from).replace("{to}", to); 
	}
	
	public String getSetProfileFromToOtherMessage(String player, String from, String to) {
		return getMessage("set-profile-from-to-other", "&6Set the profile of &7{player}&6 from &8{from}&6 to &7{to}&6.")
				.replace("{player}", player).replace("{from}", from).replace("{to}", to);
	}
	
	public String getVirtualStorageNotEnabledMessage() {
		return getMessage("virtual-storage-not-enabled", "&6Virtual storage has not been enabled");
	}
	
	public String getPlayerCommandMessage() {
		return getMessage("player-only-command", "&cThis command cannot be run by console!");
	}
	
	public String getOpeningVirtualStorageMessage(int nr) {
		return getMessage("opening-virtual-storage", "&6Opening virtual storage chest number &7{nr}").replace("{nr}", String.valueOf(nr));
	}
	
	public String getOpeningVirtualStorageOtherMessage(String name, int nr) {
		return getMessage("opening-virtual-storage-other", "&6Opening virtual storage of &8{name}&6 at number &7{nr}" + nr)
				.replace("{nr}", String.valueOf(nr)).replace("{name}", name);
	}
	
	public String getIncorrectNumberMessage(int nr) {
		return getMessage("incorrect-number", "&cIncorrect number entered:&7{nr}").replace("{nr}", String.valueOf(nr));
	}
	
	public String getClearingVirtualStorageMessage(int nr) {
		return getMessage("clearing-virtual-storage", "&6Clearing virtual chest #&8{nr}").replace("{nr}", String.valueOf(nr));
	}
	
	public String getClearingVirtualStorageOtherMessage(String name, int nr) {
		return getMessage("clearing-virtual-storage-other",  "&6Clearing &7{player}&6's virtual chest #&8{nr}")
				.replace("{nr}", String.valueOf(nr)).replace("{player}", name);
	}
	
	public String getMessage(String path, String def) {
		return ChatColor.translateAlternateColorCodes('&', DP.getConfig().getString("messages." + path, def));
	}
	
	public VirtualStorageSettings getVirtualStorageSettings() {
		return vsSettings;
	}
	
	public static enum LeftOverAction {
		DESTROY, STORE, DROP, VIRTUALSTORE
	}

}
